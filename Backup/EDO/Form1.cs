using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Edo
{

	public delegate float MFunc(params float[] args);

	public partial class Form1 : Form
	{
		List<PointF> puntos = new List<PointF>();
		float Xo, Yo;
		bool xo = false, yo = false;

		public Form1()
		{
			InitializeComponent();
		}

		public void Get_Points(MFunc f1, MFunc f2, float h)
		{
			puntos.Add(new PointF(Xo, Yo));
			for (int i = 1; i < 5000; i++)
			{
				PointF anterior = puntos[puntos.Count - 1];

				float Xn = anterior.X + f1(anterior.X, anterior.Y) * h;
				float Yn = anterior.Y + f2(anterior.X, anterior.Y) * h;

				puntos.Add(new PointF(Xn , Yn ));
			}
		}
		
		public float Func_1(params float[] args)
		{
			return 2 * args[0] + 8 * args[1];
		}
		
		public float Func_2(params float[] args)
		{
			return -1 * args[0] + 3 * args[1];
		}

		private void Form1_ResizeEnd(object sender, EventArgs e)
		{
			pictureBox1.Width = this.Width - 20;
			pictureBox1.Height = this.Height - 20;
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			try
			{
				Xo = float.Parse(textBox1.Text);
				xo = true;
			}
			catch (Exception) { xo = false; }
			
			button1.Enabled = (xo && yo) ? true : false;
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{
			try
			{
				Yo = float.Parse(textBox2.Text);
				yo = true;
			}
			catch (Exception) { yo = false; }

			button1.Enabled = (xo && yo) ? true : false;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Get_Points(Func_1, Func_2, 0.001f);
			pictureBox1.Refresh();
		}

		private void pictureBox1_Paint(object sender, PaintEventArgs e)
		{
			if (puntos.Count > 0)
			{
				List<PointF> temp = new List<PointF>();
				foreach (PointF var in puntos)
				{
					temp.Add(new PointF(var.X + pictureBox1.Width / 2, var.Y + pictureBox1.Height / 2));
				}
				e.Graphics.DrawLines(Pens.Black, temp.ToArray());
				puntos.Clear();
			}

			Pintar_Eje_de_Coordenadas(e.Graphics);

		}

		private void Pintar_Eje_de_Coordenadas(Graphics graphics)
		{

			graphics.DrawLine(Pens.Red, new Point(pictureBox1.Width / 2, 0), new Point(pictureBox1.Width / 2, pictureBox1.Height));
			graphics.DrawLine(Pens.Red, new Point(0, pictureBox1.Height / 2), new Point(pictureBox1.Width, pictureBox1.Height / 2));
		}
	}
}