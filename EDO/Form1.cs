using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Edo
{

	public delegate float MFunc(params float[] args);

	public partial class Form1 : Form
	{

		List<PointF> puntos1 = new List<PointF>();
		float Xo, Yo , h;
		bool xo = false, yo = false , ho = false;

		public Form1()
		{
			InitializeComponent();
		}

        public void Get_Points(MFunc f1, MFunc f2, float h)
        {
            //calcula los puntos en paralelo usando todos los micros que tenga la maquina donde se ejecute la aplicacion
            Parallel.Invoke(() =>
            {
                puntos1.Clear();
                puntos1.Add(new PointF(Xo, Yo));
                for (int i = 0; i < 10000000; i++)
                {
                    PointF anterior = puntos1[puntos1.Count - 1];
                    float xn = anterior.X + f1(anterior.X, anterior.Y) * h;
                    float yn = anterior.Y + f2(anterior.X, anterior.Y) * h;
                    puntos1.Add(new PointF(xn, yn));
                    if (Math.Abs(xn) > pictureBox1.Width / 2 || Math.Abs(yn) > pictureBox1.Height / 2) break;
                }
            });
        }
		
		public float Func_1(params float[] args)
		{
			return 2 * args[0] + 8 * args[1];
		}
		
		public float Func_2(params float[] args)
		{
			return -1 * args[0] + 3 * args[1];
		}

        private void Form1_ResizeEnd(object sender, EventArgs e)
		{
			pictureBox1.Width = this.Width - 20;
			pictureBox1.Height = this.Height - 20;
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			try
			{
				Xo = float.Parse(textBox1.Text);
				xo = true;
			}
			catch (Exception) { xo = false; }
			
			button1.Enabled = (xo && yo && ho) ? true : false;
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{
			try
			{
				Yo = float.Parse(textBox2.Text);
				yo = true;
			}
			catch (Exception) { yo = false; }

			button1.Enabled = (xo && yo && ho) ? true : false;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Get_Points(Func_1, Func_2, h);
			pictureBox1.Refresh();
		}

		private void pictureBox1_Paint(object sender, PaintEventArgs e)
		{
            Pintar_Eje_de_Coordenadas(e.Graphics);

			if (puntos1.Count > 0)
			{

				List<PointF> temp = new List<PointF>();
                e.Graphics.TranslateTransform(pictureBox1.Width / 2, pictureBox1.Height / 2);
				foreach (PointF var in puntos1)
				{
					temp.Add(new PointF(var.X , -var.Y ));
				}

                for (int i = 1; i < temp.Count; i++)
                {
                    e.Graphics.DrawLine(Pens.Black, temp[i - 1], temp[i]);
                }
			}

		}

		private void Pintar_Eje_de_Coordenadas(Graphics graphics)
		{

			graphics.DrawLine(Pens.Red, new Point(pictureBox1.Width / 2, 0), new Point(pictureBox1.Width / 2, pictureBox1.Height));
			graphics.DrawLine(Pens.Red, new Point(0, pictureBox1.Height / 2), new Point(pictureBox1.Width, pictureBox1.Height / 2));
		}

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                h = float.Parse(textBox3.Text);
                if (h < 0) throw new Exception();
                ho = true;
            }
            catch (Exception)
            {
               ho = false;
            }
            button1.Enabled = (xo && yo && ho) ? true : false;
        }
	}
}